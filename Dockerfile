# using technique similar to the following
# https://dev.to/ordigital/nvidia-525-cuda-118-python-310-pytorch-gpu-docker-image-1l4a
FROM jfrog-artifactory.rockhowse.in/docker-cuda-python/cuda-12.3-python-3.10:latest

# assumes we have the expected debian packages
# at /builds/rockhowse/nccl/build/pkg/deb/

# install the nccl packages
RUN dpkg -i /builds/rockhowse/nccl/build/pkg/deb/*.deb
